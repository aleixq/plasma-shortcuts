Shortcuts Plasma Applet
----------------------

A plasmoid to show the current app shortcuts, or the one manually picked. All shortcuts must be entered manually by now

If you need to bulk edit the shortcuts/apps, you can also edit directly with excellent db browser for sqlite app from https://sqlitebrowser.org/ .

It will be great to read the shortcuts defined for each app, or from kde, but is not implemented as it's just qml/js/sqlite and I couldn't find an
api to consume it, I suppose that it can be done with some cpp there. So MR's are welcome.



-- Build instructions --

cd /where/your/applet/is/generated
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=MYPREFIX ..
make
make install

(MYPREFIX is where you install your Plasma setup, replace it accordingly) to something like:
cmake ../ -DCMAKE_INSTALL_PREFIX=$PWD/install

Restart plasma to load the applet
(in a terminal type: 
kquitapp plasmashell 
and then
plasmashell)

or view it with 
plasmoidviewer -a YourAppletName
or
plasmoidviewer -a package

-- Tutorials and resources --
The explanation of the template
https://techbase.kde.org/Development/Tutorials/Plasma5/QML2/GettingStarted

Plasma QML API explained
https://techbase.kde.org/Development/Tutorials/Plasma2/QML2/API


-- Namespace adaption --

Each Plasma plugin has a unique identifier, which is also used to find related
resources (like the translation catalogs).
To avoid naming collisions, Plasma plugins use a reverse domain name notation
for that identifier:

* org.kde.plasma.* - plugins coming from Plasma modules
* org.kde.*        - plugins coming from other software from KDE
* $(my.domain).*   - plugins of your 3rd-party

The generated code uses the "org.kde.plasma" namespace for the plugin identifier.
As this namespace is reserved for use by plugins part of Plasma modules, you will
need to adapt this namespace if you are writing a plugin which is not intended to
end up in the Plasma modules.

As I work in communia.org I choose that namespace, but it can also be changed, of course.
