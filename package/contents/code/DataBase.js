
function init()
{
    var db = LocalStorage.openDatabaseSync("Shortcuts", "1.0", "Database shortcuts", 1000000);

    try {
        db.transaction(function (tx) {
             tx.executeSql('CREATE TABLE IF NOT EXISTS applications (appId integer PRIMARY KEY, appName TEXT, display TEXT)');
             tx.executeSql('CREATE TABLE IF NOT EXISTS shortcuts (sid integer PRIMARY KEY, name TEXT, shortcut TEXT, icon TEXT, shortcutGroup TEXT, appId integer)');

        })
    } catch (err) {
        console.log("Error creating table in database: " + err)
    };
}

function getHandle()
{
    try {
        var db = LocalStorage.openDatabaseSync("Shortcuts", "1.0", "Database shortcuts", 1000000);
    } catch (err) {
        console.log("Error opening database: " + err)
    }
    return db
}

function insertApp(appName, display)
{
    var db = getHandle()
    var rowid = 0;
    db.transaction(function (tx) {
        tx.executeSql('INSERT INTO applications (appName, display) VALUES( ?, ?)',
                      [appName, display])
        var result = tx.executeSql('SELECT- last_insert_rowid()')
        rowid = result.insertId
        getAllApps()
    })
    return rowid;
}

function updateApp(appId, appName, display)
{
    var db = getHandle()
    var rowid = 0;
    db.transaction(function (tx) {
        tx.executeSql('update applications set appName = ?, display = ? where appId= ?', [appName, display, appId])

        var result = tx.executeSql('SELECT- last_insert_rowid()')
        rowid = result.insertId
        getAllApps()
    })
    return rowid;
}

function getAllApps()
{
    var db = getHandle()
    db.transaction(function (tx) {
        var results = tx.executeSql(
                    'SELECT * FROM applications order by appId desc')
        appsModel.clear()
        for (var i = 0; i < results.rows.length; i++) {
            //console.log(JSON.stringify(results.rows.item(i)));
            appsModel.append(results.rows.item(i))
        }
    })
}

function deleteApp(appId)
{
    var db = getHandle()
    db.transaction(function (tx) {
        //console.log(appId)
        let deleteResults = tx.executeSql('delete from applications where appId = ?', [appId])
        let deleteShortcutsResults = tx.executeSql('delete from shortcuts where appId = ?', [appId])
        //console.log(JSON.stringify(deleteResults))
        getAllApps();
    })
}


function getAppShortcuts(appId)
{
    if (!(appId > 0) ) {
        shortcutsModel.clear()
        return
    }
    var db = getHandle()
    db.transaction(function (tx) {
        var results = tx.executeSql(
                    'SELECT * FROM shortcuts WHERE appId=? order by sid asc', [ appId ] )
        shortcutsModel.clear()
        for (var i = 0; i < results.rows.length; i++) {
            //console.log(results.rows.item(i).name)
            shortcutsModel.appendRow(results.rows.item(i))
        }
    })
}

function updateShortcut(appId, sid, column, value)
{
    var db = getHandle()
    db.transaction(function (tx) {
        var updateResults = tx.executeSql('update shortcuts set ' + column + ' = ? where sid= ?', [value, sid])
        //console.log(JSON.stringify(updateResults))
        getAppShortcuts(appId);
    })
}

function deleteShortcut(appId, sid)
{
    var db = getHandle()
    db.transaction(function (tx) {
        let deleteResults = tx.executeSql('delete from shortcuts where sid = ?', [sid])
        //console.log(JSON.stringify(deleteResults))
        getAppShortcuts(appId);
    })
}

function insertShortcut(appId)
{
    var db = getHandle()
    db.transaction(function (tx) {
        tx.executeSql('insert into shortcuts (appId, name, shortcut, icon, shortcutGroup) VALUES (?,"pending", "pending","pending", "pending")', [appId])
        getAppShortcuts(appId);
    })
}
