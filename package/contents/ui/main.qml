/*
 *    SPDX-FileCopyrightText: %{CURRENT_YEAR} %{AUTHOR} <%{EMAIL}>
 *    SPDX-License-Identifier: LGPL-2.1-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.components 3.0 as PlasmaComponents3
import QtQuick.Controls 2.15 as QQC2
import org.kde.taskmanager 0.1 as TaskManager

import QtQuick.LocalStorage 2.15
import "../code/DataBase.js" as DB


Item {
    id: root
    property var activeTaskLocal: null
    /*
     * Plasmoid.onStatusChanged: {
     s witch (Plasmoid.status)*{
     case 4: // ActiveStatus = 2 https://invent.kde.org/frameworks/plasma-framework/-/blob/master/src/plasma/plasma.h#L270
         dialo.visible = true
         break
     default:
         dialo.visible=false
         break
}
}
*/
    // Plasmoid.hideOnWindowDeactivate: false // TODO sticky

    Plasmoid.fullRepresentation:
    Flow {
        id: fulled
        Layout.minimumWidth: plasmoid.screenGeometry.width * 0.60
        Layout.minimumHeight: plasmoid.screenGeometry.height * 0.60
        ColumnLayout {
            anchors.fill: parent
            //
            //
            // MODEL
            //
            TaskManager.TasksModel {
                property string lastActiveTask : "undefined"
                id: tasksModel
                sortMode: TaskManager.TasksModel.SortVirtualDesktop
                groupMode: TaskManager.TasksModel.GroupDisabled

                filterByScreen: false

                onActiveTaskChanged: {
                    updateActiveWindowInfo()
                }
                onDataChanged: {
                    updateActiveWindowInfo()
                }
                onCountChanged: {
                    updateActiveWindowInfo()
                }

                function updateActiveWindowInfo() {

                    if (activeTaskIndex == tasksModel.activeTask )
                        return

                        var activeTaskIndex = tasksModel.activeTask

                        // fallback for Plasma 5.8
                        var abstractTasksModel = TaskManager.AbstractTasksModel || {}
                        var isActive = abstractTasksModel.IsActive || 271
                        var appName = abstractTasksModel.appName || 258
                        var isMaximized = abstractTasksModel.IsMaximized || 276
                        var virtualDesktop = abstractTasksModel.VirtualDesktop || 286


                        if (!tasksModel.data(activeTaskIndex, isActive)) {
                            activeTaskLocal = {}
                        } else {
                            activeTaskLocal = {
                                display: tasksModel.data(activeTaskIndex, Qt.DisplayRole),
                                decoration: tasksModel.data(activeTaskIndex, Qt.DecorationRole),
                                appName: tasksModel.data(activeTaskIndex, appName),
                                IsMaximized: tasksModel.data(activeTaskIndex, isMaximized),
                                VirtualDesktop: tasksModel.data(activeTaskIndex, virtualDesktop)
                            }
                            var activeTaskExists = activeTaskLocal.display !== undefined
                            if (activeTaskExists) {
                                // Simulate the task  when using plasmoidviewer:
                                //{"display":"Visor de plasmoides","decoration":"","appName":"Plasmoidviewer","IsMaximized":false,"VirtualDesktop":true}
                                //{"display":"QQmlEngine Class | Qt QML 5.15.10 - Mozilla Firefox","decoration":"","appName":"Firefox Developer Edition","IsMaximized":true,"VirtualDesktop":true}
                                //activeTaskLocal = {"display":"~ : plasmashell — Konsole","decoration":"","appName":"Konsole","IsMaximized":true,"VirtualDesktop":true};
                                if (autoButton.checked) {
                                    selectedApp.currentIndex = selectedApp.find(activeTaskLocal.appName)
                                    lastActiveTask = activeTaskLocal.appName
                                }

                            }

                        }


                }
            }



            Row {
                Layout.alignment: Qt.AlignCenter



                QQC2.ComboBox {
                    id: selectedApp
                    focus: true
                    currentIndex: indexOfValue(plasmoid.configuration.selectedApp)
                    textRole: "appName"
                    valueRole: "appId"
                    displayText: currentIndex == -1 ? "undefined" : currentText
                    model: AppsModel { }
                    onCurrentValueChanged: {
                        shortcutsList.model.getAppShortcuts(selectedApp.currentValue)
                    }
                    Plasmoid.onUserConfiguringChanged: {
                        if (!plasmoid.userConfiguring) {
                            selectedApp.model.refresh()
                            selectedApp.currentIndex = indexOfValue(plasmoid.configuration.selectedApp)
                        }
                    }
                }
                QQC2.Button {
                    id: autoButton
                    icon.name: "view-refresh"
                    checkable: true
                    focus: false
                    checked: plasmoid.configuration.automaticRefresh
                    onCheckedChanged: {
                        plasmoid.configuration.automaticRefresh = checked
                    }
                    QQC2.ToolTip {
                        visible: autoButton.hovered
                        delay: Qt.styleHints.mousePressAndHoldInterval
                        text: i18n("Automatic set the current application. Now detected: ") + tasksModel.lastActiveTask
                    }
                }

            }
            GridView {
                id: shortcutsList
                model: ShortcutsModel {
                    id: shortcutsModel
                }
                delegate: ShortcutItem {
                    id: shortcutItem
                }

                verticalLayoutDirection: GridView.TopToBottom
                flow: GridView.TopToBottom
                layoutDirection: Qt.LeftToRight
                flickableDirection: Flickable.HorizontalFlick
                clip:true
                Layout.fillWidth: true
                Layout.fillHeight: true
                cellHeight:30
            }
            // The delegate for each section header
            Component {
                // TODO
                id: sectionHeading
                Rectangle {
                    width: container.width
                    height: childrenRect.height
                    color: "lightsteelblue"

                    required property string section

                    Text {
                        text: parent.section
                        font.bold: true
                        font.pixelSize: 20
                    }
                }
            }
            /*
             *      T ext{    **                 *                                                        *
             *      id: t_output
             *      Layout.fillWidth: true
             *      textFormat: TextEdit.MarkdownText
             *      text: "
             *      ## Xdebug:
             *
             *      - Start: F5 <kbd>Ctrl</kbd>
             *      - Stop: F4
             *      - Stop at cursor: F6
             *      - Step over:
             *      - Step into: Shift+F8
             *      - Close: F5 x
             *      - Detach: F5 d
             *      - Set Brakpoint: F10
             *      - Get context: F10 x
             *      - Eval under cursor: F10 c
             *      - Eval visual:F10 e
             *      "
        }
        */
            /**
             *      d ebug-exe*cute-fro*m-cursor.*svg                                                     *
             *      debug-execute-to-cursor.svg
             *      debug-run-cursor.svg
             *      debug-run.svg
             *      debug-step-instruction.svg
             *      debug-step-into-instruction.svg
             *      debug-step-into.svg
             *      debug-step-out.svg
             *      debug-step-over.svg
             *
             */
        }
    }
    Component.onCompleted: {
        DB.init()
    }

}
