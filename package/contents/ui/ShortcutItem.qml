import QtQuick 2.15
import QtQuick.Layouts 1.15
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3


 Column {
            id: shortcutItem
            property string icon
            property string name
            property string shortcutKeys
            property string shortcutGroup


            icon: shortcutsModel.data(shortcutsModel.index(index,shortcutsModel.columnNames["icon"]),"display") ? shortcutsModel.data(shortcutsModel.index(index,shortcutsModel.columnNames["icon"]),"display") : ""
            name: shortcutsModel.data(shortcutsModel.index(index,shortcutsModel.columnNames["name"]),"display") ? shortcutsModel.data(shortcutsModel.index(index,shortcutsModel.columnNames["name"]),"display") : ""
            shortcutKeys: shortcutsModel.data(shortcutsModel.index(index,shortcutsModel.columnNames["shortcut"]),"display") ? shortcutsModel.data(shortcutsModel.index(index,shortcutsModel.columnNames["shortcut"]),"display") : ""
            shortcutGroup: shortcutsModel.data(shortcutsModel.index(index,shortcutsModel.columnNames["shortcutGroup"]),"display") ? shortcutsModel.data(shortcutsModel.index(index,shortcutsModel.columnNames["shortcutGroup"]),"display") : ""
            Row {
                spacing: units.smallSpacing
                PlasmaCore.IconItem {
                    // source - the icon to be displayed
                    source: shortcutItem.icon
                    // height & width set to equal the size of the parent item (the empty "Item" above)
                    height:shortcutText.height
                    width:units.iconSizes.small
                }
                PlasmaComponents3.Label{
                    id: shortcutText
                    Layout.fillWidth: true
                    textFormat: TextEdit.MarkdownText
                    text: {
                        return shortcutItem.name + ":   <kbd>" + shortcutItem.shortcutKeys + "</kbd>" //    *(group: " +   shortcutItem.shortcutGroup + ")*"
                    }

                    MouseArea{
                        anchors.fill: parent
                    }
                }
            }
        }
