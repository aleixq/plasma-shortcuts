import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import "../code/DataBase.js" as DB

ListModel {
    id: appsModel
    Component.onCompleted: DB.getAllApps()
    function refresh() {
         DB.getAllApps()
    }
    function deleteApp(appId) { // TODO change in qt6 when sqlmodeldata lets set directly
        DB.deleteApp(appId)
    }
    function insertApp(appName, display) { // TODO change in qt6 when sqlmodeldata lets set directly
        DB.insertApp(appName, display)
    }
    function updateApp(appId, appName, display) { // TODO change in qt6 when sqlmodeldata lets set directly
        DB.updateApp(appId, appName, display)
    }
}

