import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import "../code/DataBase.js" as DB
import Qt.labs.qmlmodels 1.0

TableModel {
    id: shortcutsModel
    property variant columnNames: {
        "sid": 0 ,
        "name": 1,
        "shortcut": 2,
        "icon": 3,
        "shortcutGroup": 4,
        "appId": 5
    }

    TableModelColumn { display: i18n("sid") }
    TableModelColumn { display: i18n("name") }
    TableModelColumn { display: i18n("shortcut") }
    TableModelColumn { display: i18n("icon") }
    TableModelColumn { display: i18n("shortcutGroup") }
    TableModelColumn { display: i18n("appId") }

    function getHeaders() {
        return columnNames
    }

    function columnNamesPosition() {
        return Object.keys(columnNames)
    }

    function getAppShortcuts(appId){
        DB.getAppShortcuts(appId)
    }
    function setLM(appId, sid, role, value) { // TODO change in qt6 when sqlmodeldata lets set directly
        DB.updateShortcut(appId, sid, role, value)
    }
    function deleteShortcut(appId, sid) { // TODO change in qt6 when sqlmodeldata lets set directly
        DB.deleteShortcut(appId, sid)
    }
    function insertShortcut(appId) { // TODO change in qt6 when sqlmodeldata lets set directly
        DB.insertShortcut(appId)
    }
    /* When delegating:
      - index: the column id.
      - row: the row id.
      - column: the column id
      - display: the above role.
    */
    /* public properties:
     - columns: list of columns as objects
    */
}

