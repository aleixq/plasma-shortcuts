/*
 *    SPDX-FileCopyrightText: 2022 Aleix Quintana Alsius <kinta@communia.org>
 *
 *    SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.15
import org.kde.kirigami 2.19 as Kirigami
import "../code/DataBase.js" as DB
import QtQuick.Controls 2.15 as QQC2
import QtQuick.LocalStorage 2.15
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0

ColumnLayout{
    property alias cfg_automaticRefresh: automaticRefresh.checked
    property alias cfg_selectedApp: selectedApp.currentValue

    Kirigami.FormLayout{
        QQC2.CheckBox {
            id: automaticRefresh
            Kirigami.FormData.label: i18n("App detection:")
            text: i18n("Automatic refresh")
        }
    }
    RowLayout {
        Layout.alignment: Qt.AlignHCenter
        QQC2.ComboBox {
            id: selectedApp
            focus: true
            currentIndex: indexOfValue(plasmoid.configuration.selectedApp)
            textRole: "appName"
            valueRole: "appId"
            model: AppsModel {
                id: appModel
            }
            onActivated: {
                shortcutsTable.model.getAppShortcuts(currentValue)
            }

        }
        QQC2.Button {
            id: removeApp
            icon.name: "edit-delete"
            enabled: selectedApp.currentIndex != -1
            onClicked: {
                appModel.deleteApp(selectedApp.currentValue)
            }
            QQC2.ToolTip {
                visible: removeApp.hovered
                delay: Qt.styleHints.mousePressAndHoldInterval
                text: i18n("Remove application")
            }
        }

        QQC2.Button {
            id: updateApp
            enabled: selectedApp.currentIndex != -1
            icon.name: "document-edit"
            onClicked: {
                appNameDialog.update = true
                appNameDialog.name = selectedApp.currentText
                appNameDialog.display = "" // TODO discover display preexisting value: appModel.data(appModel.index(selectedApp.currentValue,0),"display")
                appNameDialog.open()
            }
            QQC2.ToolTip {
                visible: updateApp.hovered
                delay: Qt.styleHints.mousePressAndHoldInterval
                text: i18n("Edit application")
            }
        }
        QQC2.Button {
            id: addApp
            icon.name: "window-new"
            onClicked: {
                appNameDialog.update = false
                appNameDialog.name = ""
                appNameDialog.display = ""

                appNameDialog.open()
            }
            QQC2.ToolTip {
                visible: addApp.hovered
                delay: Qt.styleHints.mousePressAndHoldInterval
                text: i18n("Add application")
            }
        }
        Kirigami.PromptDialog {
            id: appNameDialog
            title: i18n("App name")
            property bool update: false
            property string name
            property string display

            standardButtons: Kirigami.Dialog.NoButton
            customFooterActions: [
            Kirigami.Action {
                text: appNameDialog.update ? i18n("Update App") : i18n("Create App")
                iconName: "dialog-ok"
                onTriggered: {
                    if (appNameDialog.update){
                        appModel.updateApp(selectedApp.currentValue, createdAppName.text, createdDisplay.text)
                    } else {
                        appModel.insertApp(createdAppName.text, createdDisplay.text)
                    }
                    showPassiveNotification(appNameDialog.update ? i18n("App Updated") : i18n("App Created"));
                    appNameDialog.close();
                }
            },
            Kirigami.Action {
                text: qsTr("Cancel")
                iconName: "dialog-cancel"
                onTriggered: {
                    appNameDialog.close();
                }
            }
            ]

            Column {
                QQC2.TextField {
                    id: createdAppName
                    placeholderText: qsTr("App name...")
                    text: appNameDialog.update? appNameDialog.name : ""
                }
                QQC2.TextField {
                    id: createdDisplay
                    placeholderText: qsTr("Display name...")
                    text: appNameDialog.update? appNameDialog.display : ""
                }
            }
        }

    }
    Item  {
        Layout.fillHeight: true
        Layout.fillWidth: true
        clip:true
        QQC2.HorizontalHeaderView {
            id: horizontalHeader
            syncView: shortcutsTable
            anchors.left: shortcutsTable.left
            model: shortcutsEditableModel.columnNamesPosition()
            width: parent.width
        }
        QQC2.VerticalHeaderView {
            id: verticalHeader
            syncView: shortcutsTable
            //anchors.right: shortcutsTable.left
            model: shortcutsTable.rows
        }

        TableView {
            //https://github.com/gabrielchristo/qml-tableview/blob/master/main.qml
            id: shortcutsTable
            anchors.fill: parent
            //width: 0.85 * parent.width; height: 0.8 * parent.height
            columnSpacing: 1
            rowSpacing: 1
            clip: true


            model: ShortcutsModel {
                id: shortcutsEditableModel
            }
            // scrollbar config
            //ScrollBar.horizontal: QQC2.ScrollBar{
            //policy: "AlwaysOn"
            //}
            //ScrollBar.vertical: QQC2.ScrollBar{
            //policy: "AlwaysOn"
            //}
            //ScrollIndicator.horizontal: QQC2.ScrollIndicator { }
            //ScrollIndicator.vertical: QQC2.ScrollIndicator { }
            leftMargin: verticalHeader.width
            topMargin: horizontalHeader.height

            delegate:
            DelegateChooser {

                DelegateChoice { column: 0
                    QQC2.Button {
                        id: remover
                        icon.name: "edit-delete-remove"
                        flat: true
                        onClicked: {
                            let appId = shortcutsEditableModel.data(shortcutsEditableModel.index(row, 5),"display")
                            shortcutsEditableModel.deleteShortcut(appId, modelData)
                        }
                        QQC2.ToolTip {
                            visible: remover.hovered
                            delay: Qt.styleHints.mousePressAndHoldInterval
                            text: i18n("remove shortcut definition")
                        }

                    }
                }
                DelegateChoice {
                    Item {
                        QQC2.Label {
                            id: fieldValue
                            width: parent.width
                            text: model.display
                            anchors.centerIn: parent
                            padding:10
                        }

                        MouseArea {
                            id: cellMouseArea
                            anchors.fill: parent
                            onClicked: {
                                if( (index >= 0) ){
                                    loader.visible = true
                                    loader.item.forceActiveFocus()
                                }

                            }
                        }

                        Loader {
                            id: loader
                            height: parent.height
                            width: parent.width
                            visible: false
                            sourceComponent: visible ? input : undefined
                            Component {
                                id: input
                                QQC2.TextField {
                                    text: display
                                    validator: RegExpValidator { regExp: /^[^"'>]+$/ }
                                    Keys.onReturnPressed: { focus = false }
                                    onEditingFinished:{
                                        loader.visible = false
                                        let sid = shortcutsEditableModel.data(shortcutsEditableModel.index(row, 0),"display")
                                        let appId = shortcutsEditableModel.data(shortcutsEditableModel.index(row, 5),"display")
                                        let columnName = shortcutsEditableModel.columns[column].display

                                        shortcutsEditableModel.setLM(appId, sid, columnName, text) // TODO change in qt6 when sqlmodeldata lets set directly
                                    }
                                    QQC2.ToolTip {
                                        visible: pressed
                                        delay: Qt.styleHints.mousePressAndHoldInterval
                                        text: i18n("shift+enter to save value")
                                    }

                                    onActiveFocusChanged: {
                                        if (!activeFocus) {
                                            loader.visible = false
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    QQC2.Button {
        id: insertRow
        icon.name: "insert-table-row"
        enabled: selectedApp.currentIndex != -1
        onClicked: {
            shortcutsEditableModel.insertShortcut(selectedApp.currentValue)
        }
        QQC2.ToolTip {
            visible: insertRow.hovered
            delay: Qt.styleHints.mousePressAndHoldInterval
            text: i18n("Append shortcut")
        }
    }
}
